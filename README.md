[![pipeline status](https://gitlab.inria.fr/auctus-team/auctus-team.gitlabpages.inria.fr/badges/master/pipeline.svg)](https://gitlab.inria.fr/auctus-team/auctus-team.gitlabpages.inria.fr)
---

Auctus Hugo website using GitLab Pages.

https://auctus-team.gitlabpages.inria.fr/

To edit content to this website go [here](https://gitlab.inria.fr/auctus-team/website/content) (only available for Auctus team members)
